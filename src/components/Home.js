import React, { Component } from 'react';
// import $ from 'jquery';
import './Login.css';

class Home extends Component {

    render() {
        return (
            <div className="lemonadaFont">
                <h1 className="page-header text-center">Welcome {this.props.userData.username}</h1>
                <h2>FullName: {this.props.userData.fullname}</h2>
                <h2>Email: {this.props.userData.email}</h2>
                <h2>Phone: {this.props.userData.phone}</h2>
            </div>
        )
    }

}

export default Home;