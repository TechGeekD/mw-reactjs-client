import React, { Component } from 'react';
import $ from 'jquery';
import './Login.css';

class Login extends Component {

    login(e) {

        var userAuth = {
            username: this.refs.username.value,
            password: this.refs.password.value
        }
        
        var settings = {
            "url": "http://safe-crag-74723.herokuapp.com/api/login",
            "method": "POST",
            "headers": {
                "content-type": "application/json",
            },
            "data": JSON.stringify(userAuth)
        }
  
        $.post(settings, userAuth).done((data) => {
            console.log(data);
            if ( data.res ) {
                console.log('Login Success');    
                this.props.checkLogin(true, data.res);
            } else {
                this.props.checkLogin(false, data.res);
                console.log('Failed To Login');
            }
        }).fail((jqXhr) => {
            console.log('Failed To Login');
            this.props.checkLogin(false, 'null');
        });
        
        e.preventDefault();
    }

    render() {
        return (
            <div className="container-fluid lemonadaFont">
                <div>
                    <h1 className="page-header">Metta Welfare</h1>
                </div>
                <div className="jumbotron rounded loginBox">
                    <h2 className="loginBoxHeader">Login</h2>
                    <form onSubmit={this.login.bind(this)}>
                        <div className="form-group">
                            <label htmlFor="username">Enter Username:</label>
                            <input  type="text" className="form-control" 
                                    ref='username' placeholder="Username"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="username">Enter Password:</label>
                            <input  type="password" className="form-control" 
                                    ref='password' placeholder="Password"/>
                        </div>
                        <button type="submit" className="submit">Submit</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Login;