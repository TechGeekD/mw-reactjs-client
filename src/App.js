import React, { Component } from 'react';
import Login from './components/Login';
import Home from './components/Home';
import './App.css';

class App extends Component {

  constructor() {
    super();
    this.state = {
         loginStatus: false,
         userdata: { }
    }
  }

  handleLoginStat(loginStat, data){
    console.log(data);
    this.setState({
      loginStatus: loginStat,
      userData: data
    })
  }
  
  render() {

    if ( this.state.loginStatus ) {
      return (
          <Home userData={this.state.userData}></Home>
      );
    } else {
      return (
          <Login checkLogin={this.handleLoginStat.bind(this)} loginStatus={this.state.loginStatus}></Login>
      );
    }
  }
}

export default App;
